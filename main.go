package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/lann/mpris2-go"
)

func main() {
	conn, err := mpris2.Connect() // Connect to DBus

	mp, err := conn.GetAnyMediaPlayer()
	if err != nil {
		log.Fatalf("no player: %v", err)
	}

	meta, err := mp.Metadata()
	artists := strings.Join(meta.Artists(), ", ")
	fmt.Printf("%s - %s ", artists, meta.Title())
}
