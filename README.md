# Spotify Playing

Quickly list what Spotify is playing leveraging the MPRIS-2 interface to the 
player. The application doesn't check if a music player is running. This is
left by to for example `pgrep`.

`pgrep && spotify-playing`

The output is limited, but suffices for my use case. MRs welcome.
